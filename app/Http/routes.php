<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Backend\BackendController@index');
Route::get('/login', 'LoginController@index');
Route::get('/logout', 'LoginController@logout');
Route::post('validate-login', 'LoginController@login');
Route::group(['prefix' => 'backend', 'namespace' => 'Backend','middleware' => ['verify-login']], function () {
    Route::get('/', 'BackendController@index');
    Route::group(['prefix' => 'master', 'namespace' => 'Master'], function () {
        Route::group(['prefix' => 'pelanggan'], function () {
            Route::get('/', 'PelangganController@index');
            Route::post('/form', 'PelangganController@form');
            Route::post('/save', 'PelangganController@save');
            Route::post('/delete', 'PelangganController@delete');
        });

        Route::group(['prefix' => 'pelanggaran'], function () {
            Route::get('/', 'PelanggaranController@index');
            Route::post('/form', 'PelanggaranController@form');
            Route::post('/save', 'PelanggaranController@save');
            Route::post('/delete', 'PelanggaranController@delete');
        });

        Route::group(['prefix' => 'petugas'], function () {
            Route::get('/', 'PetugasController@index');
            Route::post('/form', 'PetugasController@form');
            Route::post('/save', 'PetugasController@save');
            Route::post('/delete', 'PetugasController@delete');
        });

        Route::group(['prefix' => 'spk'], function () {
            Route::get('/', 'SpkController@index');
            Route::post('/form', 'SpkController@form');
            Route::post('/save', 'SpkController@save');
            Route::post('/delete', 'SpkController@delete');
        });

        Route::group(['prefix' => 'batd'], function () {
            Route::get('/show-by-spk', 'BatdController@showBySpk');
            Route::post('/form', 'BatdController@form');
            Route::post('/save', 'BatdController@save');
            Route::post('/delete', 'BatdController@delete');
        });


        // Route::group(['prefix'=>'realisasi'],function (){
        //     Route::get('/','RealisasiController@index');
        //     Route::post('/filter','RealisasiController@filterRealisasi');
        //     Route::get('/detail','RealisasiController@detail');


        Route::group(['prefix' => 'realisasi'], function () {
            Route::get('/', 'RealisasiController@index');
            Route::get('/detail', 'RealisasiController@detail');
            Route::post('/delete', 'RealisasiController@delete');
            Route::post('/filter','RealisasiController@filterRealisasi');
            Route::post('/add-revisi', 'RealisasiController@formRevisi');
            Route::post('/save-revisi', 'RealisasiController@saveRevisi');
            Route::post('/verifikasi','RealisasiController@verifikasi');
        });
    });

});





//Untuk menambah APi
Route::group(['prefix'=>'api','namespace'=>'Api'],function (){
    Route::post('/login','ApiLoginController@login');
    Route::get('/get-spk/{id}','ApiSpkController@getSpk');
    Route::get('/get-pelanggan/{id}','ApiPelangganController@getPelanggan');
    Route::get('/get-pelanggaran','ApiPelanggaranController@index');
    Route::post('/add-realisasi','ApiRealisasiController@addRealisasi');
    Route::get('/list-realisasi/{id}','ApiRealisasiController@listRealisasi');
    Route::get('/view-realisasi/{id}','ApiRealisasiController@viewDetailRealisasi');
    Route::get('/list-revisi-baru/{id}','ApiRealisasiController@listRealisasiRevisiBaru');
    Route::get('/view-realisasi-revisi-baru/{id}','ApiRealisasiController@viewRealisasiRevisiBaru');
    Route::get('/list-revisi-kirim/{id}','ApiRealisasiController@listRealisasiRevisiKirim');
    Route::get('/view-realisasi-revisi-kirim/{id}','ApiRealisasiController@viewRealisasiRevisiKirim');
    Route::post('/update-realisasi/{id}','ApiRealisasiController@updateRealisasiRevisi');
    Route::get('/count-baru/{id}','ApiRealisasiController@countBaru');
    Route::get('/count-revisi/{id}','ApiRealisasiController@countRevisi');
    Route::get('/count-coba/{id}','ApiCoba@countCoba');
    
    //Route::get('/get-batd-by-spk/{id}','ApiBatdController@getBatdBySpk');
    
    //Route::get('/profile/{id}','ApiProfileController@getProfile');



});


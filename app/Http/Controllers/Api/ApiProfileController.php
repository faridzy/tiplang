<?php


namespace App\Http\Controllers\Api;
use App\Classes\MessageSystemClass;
use App\Http\Controllers\Controller;
use App\Models\Petugas;

class ApiProfileController
{
	private  $messageSystem;

    public  function __construct()
    {
        $this->messageSystem= new MessageSystemClass();
    }
    
    public function getProfile($petugasId){
        $apiName='GET_PROFILE';
        $sendingParams=[
            'id'=>$petugasId
        ];
        if (is_null($petugasId)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter id_user!',json_encode($sendingParams));
        }
        $profile=Petugas::where(['id'=>$petugasId])->first();
        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Found Success!',
            'data' => [
	            'id' => $profile->id,
	            'nip'=>$profile->nip,
	            'nama'=>$profile->nama
            ]
        ];
        return response()->json($params);
    }
}
<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Clasess\MessageSystemClass;
use App\Models\Batd;
use App\Models\Pelanggan;

class ApiBatdController extends Controller
{

    private  $messageSystem;

    public  function __construct()
    {
        $this->messageSystem= new MessageSystemClass();
    }


    public function getBatdBySpk($spkId){
        $apiName='GET_BTD';
        $sendingParams=[
            'spk_id'=>$spkId
        ];
        if(is_null($spkId)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter petugas_id!',json_encode($sendingParams));
        }
        $data=Batd::where(['spk_id'=>$spkId])->orderBy('nomor_batd','ASC');
        $dataP=Pelanggan::where(['id'=>$data->pelanggan_id])->get();

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get BTD by SPK Success!',
            'data' => $data->$datap
        ];


        return response()->json($params);
    }


}
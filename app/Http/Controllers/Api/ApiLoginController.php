<?php

namespace App\Http\Controllers\Api;


use App\Clasess\MessageSystemClass;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Petugas;

class ApiLoginController extends Controller
{
    private  $messageSystem;

    public  function __construct()
    {
        $this->messageSystem= new MessageSystemClass();
    }

    public function login(Request $request){
        $apiName='VALIDATE_LOGIN';
        $nip=$request->nip;
        $password=sha1($request->password);

        $sendingParams = [
            'nip' => $nip,
            'password' => $password,
        ];

        if(is_null($nip)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter nip!',json_encode($sendingParams));
        }

        $activeUser=Petugas::where(['nip'=>$nip])->first();
        if(is_null($activeUser)){
            return $this->messageSystem->returnApiMessage($apiName, 404, "NIK not found!", json_encode($sendingParams) );
        }

        if($activeUser->password != $password){
            return $this->messageSystem->returnApiMessage($apiName, 401, "Password not match!", json_encode($sendingParams) );
        }

        $data = [
            'id' => $activeUser->id,
            'nip' => $activeUser->nip,
            'nama'=>$activeUser->nama,
            'jabatan'=>$activeUser->jabatan,
        ];

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Login Success!',
            'data' => $data
        ];


        return response()->json($params);



    }

}
<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Spk;
use App\Clasess\MessageSystemClass;

class ApiSpkController extends Controller
{

    private  $messageSystem;

    public  function __construct()
    {
        $this->messageSystem= new MessageSystemClass();
    }


    public function getSpk($petugasId){
        $apiName='GET_SPK';
        $sendingParams=[
            'petugas_id'=>$petugasId
        ];
        if(is_null($petugasId)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter petugas_id!',json_encode($sendingParams));
        }
        $data=Spk::where(['petugas_id'=>$petugasId])->orderBy('nomor_spk','ASC')->get();

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get SPK by Petugas Success!',
            'data' => $data
        ];


        return response()->json($params);
    }

}

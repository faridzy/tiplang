<?php

namespace App\Http\Controllers\Api;

 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Spk;
use App\Models\Batd;
use App\Models\Realisasi;
use App\Clasess\MessageSystemClass;

class ApiPelangganController extends Controller
{

    private  $messageSystem;

    public  function __construct()
    {
        $this->messageSystem= new MessageSystemClass();
    }


    public function getPelanggan($spkId){
        $apiName='GET_SPK';
        $sendingParams=[
            'spk_id'=>$spkId
        ];
        if(is_null($spkId)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter petugas_id!',json_encode($sendingParams));
        }

        $data=Batd::where(['spk_id'=>$spkId])->get();
        $detailPelanggan=[];
        foreach ($data as $key => $item) {
            $cekRealisai=Realisasi::where(['batd_id'=>$item->id])->first();
            if(is_null($cekRealisai)){
                $detailPelanggan[]=[
                'spk_id'=>$item->getSpk->id,
                'batd_id'=>$item->id,
                'nomor_spk'=>$item->getSpk->nomor_spk,
                'nomor_batd'=>$item->nomor_batd,
                'tanggal_batd'=>$item->tanggal_batd,
                'pelanggan_id'=>$item->getPelanggan->id,
                'nomor_pelanggan'=>$item->getPelanggan->nomor_pelanggan,
                'nama_pelanggan'=>$item->getPelanggan->nama,
                'zona'=>$item->getPelanggan->zona,
                'jalan'=>$item->getPelanggan->jalan,
            ];

            }
        }


        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get SPK by Petugas Success!',
            'data' => $detailPelanggan
        ];


        return response()->json($params);

    }
}
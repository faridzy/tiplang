<?php
 
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Clasess\MessageSystemClass;
use App\Models\Batd;
use App\Models\Spk;
use App\Models\Pelanggaran;
use App\Models\Realisasi;
use App\Models\RealisasiFoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ApiRealisasiController extends Controller
{

    private  $messageSystem;

    public  function __construct()
    {
        $this->messageSystem= new MessageSystemClass();
    }

    public function addRealisasi(Request $request){

        $apiName='ADD_REALISASI';
        $kondisiStanMeter=$request->kondisi_stan_meter;
        $catatanStanMeter=$request->catatan_stan_meter;
        $tanggalAngkat=$request->tanggal_angkat;
        $noMeter=$request->no_meter;
        $ukuranMeter=$request->ukuran_meter;
        $angkaAngkat=$request->angka_angkat;
        $merkMeter=$request->merk_meter;
        $tanggalRealisasi=date('Y-m-d');
        $batdId=$request->batd_id;
        $pelanggaranId=$request->pelanggaran_id;
        $hasil=$request->hasil;
        $picture1 = $request->file('pict1');
        $picture2 = $request->file('pict2');
        $picture3 = $request->file('pict3');
        $picture4 = $request->file('pict4');

        $sendingParams = [
            'kondisi_stan_meter' => $kondisiStanMeter,
            'catatan_stan_meter' => $catatanStanMeter,
            'tanggal_angkat'=>$tanggalAngkat,
            'no_meter'=>$noMeter,
            'ukuran_meter'=>$ukuranMeter,
            'angka_angkat'=>$angkaAngkat,
            'merk_meter'=>$merkMeter,
            'tanggal_realisasi'=>$tanggalRealisasi,
            'batd_id'=>$batdId,
            'pelanggaran_id'=>$pelanggaranId,
            'hasil'=>$hasil
        ];

        if(is_null($kondisiStanMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter kondisi_stan_meter!',json_encode($sendingParams));
        }
        if(is_null($catatanStanMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter catatan_stan_meter!',json_encode($sendingParams));
        }
        if(is_null($tanggalAngkat)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter tanggal_angkat!',json_encode($sendingParams));
        }
        if(is_null($noMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter no_meter!',json_encode($sendingParams));
        }
        if(is_null($ukuranMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter ukuran_meter!',json_encode($sendingParams));
        }
        if(is_null($angkaAngkat)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter angka_angkat!',json_encode($sendingParams));
        }
        if(is_null($merkMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter merk_meter!',json_encode($sendingParams));
        }
        if(is_null($hasil)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter hasil!',json_encode($sendingParams));
        }

        $checkBatd=Batd::where(['id'=>$batdId])->first();
        if(is_null($checkBatd)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Batd not found!',json_encode($sendingParams));
        }
        $checkPelanggaran=Pelanggaran::where(['id'=>$pelanggaranId])->first();
        if(is_null($checkPelanggaran)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Pelanggaran not found!',json_encode($sendingParams));
        }

        try{
            $data = new Realisasi();
            $data->kondisi_stan_meter=$kondisiStanMeter;
            $data->catatan_stan_meter=$catatanStanMeter;
            $data->tanggal_angkat=$tanggalAngkat;
            $data->no_meter=$noMeter;
            $data->ukuran_meter=$ukuranMeter;
            $data->angka_angkat=$angkaAngkat;
            $data->merk_meter=$merkMeter;
            $data->hasil=$hasil;
            $data->batd_id=$batdId;
            $data->pelanggaran_id=$pelanggaranId;
            $data->tanggal_realisasi=$tanggalRealisasi;
            $data->status_baru=1;
            
            if(!empty($picture1)||!empty($picture2)||!empty($picture3)||!empty($picture4)) {
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
            
            if(!is_null($picture1)){
                $filename1 = date("YmdHis").'-'.$picture1->getClientOriginalName();
                $picture1->move($destinationPath,$filename1);
                $data->pict1=$filename1;
                $data->save();
                
            }
            if (!is_null($picture2)) {
                $filename2 = date("YmdHis").'-'.$picture2->getClientOriginalName();
                $picture2->move($destinationPath,$filename2);
                $data->pict2=$filename2;
                $data->save();
            }
            if(!is_null($picture3)){
                $filename3 = date("YmdHis").'-'.$picture3->getClientOriginalName();
                $picture3->move($destinationPath,$filename3);
                $data->pict3=$filename3;
                $data->save();
                
            }
            if(!is_null($picture4)){
                $filename4 = date("YmdHis").'-'.$picture4->getClientOriginalName();
                $picture4->move($destinationPath,$filename4);
                $data->pict4=$filename4;
                $data->save();
            }
        }
            
            $data->save();

            

            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Adding Realisasi success!',
                'data' => $data
            ];

            return response()->json($params);

        }catch (\Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

    }

    public function listRealisasi($petugasId){
        $apiName='LIST_REALISASI';
        $sendingParams=[
            'petugas_id'=>$petugasId
        ];
        if(is_null($petugasId)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Realisasi not Found!',json_encode($sendingParams));
        }
        $cekPetugas=Spk::where(['petugas_id'=>$petugasId]);
        $spkId=$cekPetugas->pluck('id');
        $cekKirim=Realisasi::where(['status_baru'=>1,'status_revisi'=>0])->first();
        if($cekKirim) {
            $cekBaru=Realisasi::where(['status_baru'=>1,'status_revisi'=>0])->get();
            foreach ($cekBaru as $item){
                $id_batd[]=$item->batd_id;
            }
            $datakirim=Batd::whereIn('id',$id_batd)->where(['spk_id'=>$spkId])->first();
            if($datakirim) {
                $data = Batd::whereIn('id', $id_batd)->where(['spk_id'=>$spkId])->get();
                foreach ($data as $item) {
                    $listrealisasi[] = [
                        'id_batd' => $item->id,
                        'id_realisasi'=>$item->getRealisasi->id,
                        'nomor_batd' => $item->nomor_batd,
                        'tanggal_batd' => $item->tanggal_batd,
                        'nomor_pelanggan' => $item->getPelanggan->nomor_pelanggan,
                        'nama' => $item->getPelanggan->nama,
                        'zona'=>$item->getPelanggan->zona,
                        'jalan' => $item->getPelanggan->jalan,                  
                    ];
                }
                $params = [
                    'code' => 302,
                    'description' => 'found',
                    'messaage' => 'Realisasi berhasil di dapatkan',
                    'list' => $listrealisasi
                ];
                return response()->json($params);
            }
            else{
                $params = [
                    'code' => 500,
                    'description' => 'found',
                    'messaage' => 'Tidak ada Realisasi diterima',
                ];
                return response()->json($params);
            }
        }
        else {
            $params = [
                'code' => 500,
                'description' => 'found',
                'messaage' => 'Tidak ada Realisasi diterima',
            ];
            return response()->json($params);
        }
    }

    public function listRealisasiRevisiBaru($petugasId){
        $apiName='LIST_REALISASI BY REVISI';
        //$data=Realisasi::find($id);
        $sendingParams=[
            'petugas_id'=>$petugasId
        ];
        if(is_null($petugasId)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Realisasi not Found!',json_encode($sendingParams));
        }
        $cekPetugas=Spk::where(['petugas_id'=>$petugasId]);
        $spkId=$cekPetugas->pluck('id');
        $cekKirim=Realisasi::where(['status_baru'=>0,'status_revisi'=>1])->first();
        if($cekKirim) {
            $cekBaru=Realisasi::where(['status_baru'=>0,'status_revisi'=>1])->get();
            foreach ($cekBaru as $item){
                $id_batd[]=$item->batd_id;
            }
            $datakirim=Batd::whereIn('id',$id_batd)->where(['spk_id'=>$spkId])->first();
            if($datakirim) {
                $data = Batd::whereIn('id', $id_batd)->where(['spk_id'=>$spkId])->get();
                foreach ($data as $item) {
                    $listrealisasi[] = [
                        'id_batd' => $item->id,
                        'id_realisasi'=>$item->getRealisasi->id,
                        'nomor_batd' => $item->nomor_batd,
                        'tanggal_batd' => $item->tanggal_batd,
                        'nomor_pelanggan' => $item->getPelanggan->nomor_pelanggan,
                        'nama' => $item->getPelanggan->nama,
                        'zona'=>$item->getPelanggan->zona,
                        'jalan' => $item->getPelanggan->jalan,                     
                    ];
                }
                $params = [
                    'code' => 302,
                    'description' => 'found',
                    'messaage' => 'Realisasi berhasil di dapatkan',
                    'list' => $listrealisasi
                ];
                return response()->json($params);
            }
            else{
                $params = [
                    'code' => 500,
                    'description' => 'found',
                    'messaage' => 'Tidak ada Realisasi diterima',
                ];
                return response()->json($params);
            }
        }
        else {
            $params = [
                'code' => 500,
                'description' => 'found',
                'messaage' => 'Tidak ada Realisasi diterima',
            ];
            return response()->json($params);
        }
    }

    public function listRealisasiRevisiKirim($petugasId){
        $apiName='LIST_REALISASI BY REVISI';
        //$data=Realisasi::find($id);
        $sendingParams=[
            'petugas_id'=>$petugasId
        ];
        if(is_null($petugasId)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Realisasi not Found!',json_encode($sendingParams));
        }

        $cekPetugas=Spk::where(['petugas_id'=>$petugasId]);
        $spkId=$cekPetugas->pluck('id');
        $cekKirim=Realisasi::where(['status_baru'=>0,'status_revisi'=>2])->first();
        if($cekKirim) {
            $cekBaru=Realisasi::where(['status_baru'=>0,'status_revisi'=>2])->get();
            foreach ($cekBaru as $item){
                $id_batd[]=$item->batd_id;
            }
            $datakirim=Batd::whereIn('id',$id_batd)->where(['spk_id'=>$spkId])->first();
            if($datakirim) {
                $data = Batd::whereIn('id', $id_batd)->where(['spk_id'=>$spkId])->get();
                foreach ($data as $item) {
                    $listrealisasi[] = [
                        'id_batd' => $item->id,
                        'id_realisasi'=>$item->getRealisasi->id,
                        'nomor_batd' => $item->nomor_batd,
                        'tanggal_batd' => $item->tanggal_batd,
                        'nomor_pelanggan' => $item->getPelanggan->nomor_pelanggan,
                        'nama' => $item->getPelanggan->nama,
                        'zona'=>$item->getPelanggan->zona,
                        'jalan' => $item->getPelanggan->jalan,                  
                    ];
                }
                $params = [
                    'code' => 302,
                    'description' => 'found',
                    'message' => 'Realisasi berhasil di dapatkan',
                    'list' => $listrealisasi
                ];
                return response()->json($params);
            }
            else{
                $params = [
                    'code' => 500,
                    'description' => 'found',
                    'message' => 'Tidak ada Realisasi diterima',
                ];
                return response()->json($params);
            }
        }
        else {
            $params = [
                'code' => 500,
                'description' => 'found',
                'message' => 'Tidak ada Realisasi diterima',
            ];
            return response()->json($params);
        }
    }


    public function viewDetailRealisasi($id){
        $apiName='VIEW_REALISASI_BY_STATUS_REVISI';
        $data=Realisasi::where(['id'=>$id,'status_baru'=>1])->first();
        $sendingParams=[
            'id'=>$id,
            'status_baru'=>1
        ];
        if(is_null($data)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Realisasi Not Found!',json_encode($sendingParams));
        }
        $data=[
            'id'=>$data->id,
            'tanggal_realisasi'=>$data->tanggal_realisasi,
            'hasil'=>$data->hasil,
            'kondisi_stan_meter'=>$data->kondisi_stan_meter,
            'catatan_stan_meter'=>$data->catatan_stan_meter,
            'tanggal_angkat'=>$data->tanggal_angkat,
            'no_meter'=>$data->no_meter,
            'ukuran_meter'=>$data->ukuran_meter,
            'angka_angkat'=>$data->angka_angkat,
            'merk_meter'=>$data->merk_meter,
            'batd_id'=>$data->batd_id,
            'pelanggaran_id'=>$data->pelanggaran_id,
            'status_baru'=>$data->status_baru,
            'status_revisi'=>$data->status_revisi,
            'ket_realisasi'=>$data->ket_realisasi,
            'pict1'=>"public/uploads/foto-realisasi/".$data->pict1,
            'pict2'=>"public/uploads/foto-realisasi/".$data->pict2,
            'pict3'=>"public/uploads/foto-realisasi/".$data->pict3,
            'pict4'=>"public/uploads/foto-realisasi/".$data->pict4,
        ];     
        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'View Realisasi success!',
            'data' => $data,
                //'fotoRealisasi'=>$dataFoto
        ];
        return response()->json($params);
    }

    public function viewRealisasiRevisiBaru($id){
        $apiName='VIEW_REALISASI_BY_STATUS_REVISI';
        $data=Realisasi::where(['id'=>$id,'status_revisi'=>1])->first();
        $sendingParams=[
            'id'=>$id,
            'status_revisi'=>1
        ];
        if(is_null($data)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Realisasi By Status Revisi Not Found!',json_encode($sendingParams));
        }
        $data=[
            'id'=>$data->id,
            'tanggal_realisasi'=>$data->tanggal_realisasi,
            'hasil'=>$data->hasil,
            'kondisi_stan_meter'=>$data->kondisi_stan_meter,
            'catatan_stan_meter'=>$data->catatan_stan_meter,
            'tanggal_angkat'=>$data->tanggal_angkat,
            'no_meter'=>$data->no_meter,
            'ukuran_meter'=>$data->ukuran_meter,
            'angka_angkat'=>$data->angka_angkat,
            'merk_meter'=>$data->merk_meter,
            'batd_id'=>$data->batd_id,
            'pelanggaran_id'=>$data->pelanggaran_id,
            'status_baru'=>$data->status_baru,
            'status_revisi'=>$data->status_revisi,
            'ket_realisasi'=>$data->ket_realisasi,
            'pict1'=>"public/uploads/foto-realisasi/".$data->pict1,
            'pict2'=>"public/uploads/foto-realisasi/".$data->pict2,
            'pict3'=>"public/uploads/foto-realisasi/".$data->pict3,
            'pict4'=>"public/uploads/foto-realisasi/".$data->pict4,
        ];
        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'View Realisasi By Status Revisi success!',
            'data' => $data,               
        ];
        return response()->json($params);
    }

    public function viewRealisasiRevisiKirim($id){
        $apiName='VIEW_REALISASI_REVISI_KIRIM';
        $data=Realisasi::where(['id'=>$id,'status_revisi'=>2])->first();
        $sendingParams=[
            'id'=>$id,
            'status_revisi'=>1
        ];
        if(is_null($data)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Realisasi By Status Revisi Not Found!',json_encode($sendingParams));
        }
        $dataRealisasiFoto=RealisasiFoto::where(['realisasi_id'=>$data->id])->get();
       $data=[
            'id'=>$data->id,
            'tanggal_realisasi'=>$data->tanggal_realisasi,
            'hasil'=>$data->hasil,
            'kondisi_stan_meter'=>$data->kondisi_stan_meter,
            'catatan_stan_meter'=>$data->catatan_stan_meter,
            'tanggal_angkat'=>$data->tanggal_angkat,
            'no_meter'=>$data->no_meter,
            'ukuran_meter'=>$data->ukuran_meter,
            'angka_angkat'=>$data->angka_angkat,
            'merk_meter'=>$data->merk_meter,
            'batd_id'=>$data->batd_id,
            'pelanggaran_id'=>$data->pelanggaran_id,
            'status_baru'=>$data->status_baru,
            'status_revisi'=>$data->status_revisi,
            'ket_realisasi'=>$data->ket_realisasi,
            'pict1'=>"public/uploads/foto-realisasi/".$data->pict1,
            'pict2'=>"public/uploads/foto-realisasi/".$data->pict2,
            'pict3'=>"public/uploads/foto-realisasi/".$data->pict3,
            'pict4'=>"public/uploads/foto-realisasi/".$data->pict4,
        ];
        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'View Realisasi By Status Revisi success!',
            'data' =>  $data
        ];
        return response()->json($params);

    }

    public function updateRealisasiRevisi($realisasiId, Request $request){
        $apiName='UPDATE_REALISASI_REVISI';
        $realisasiId=$realisasiId;
        $kondisiStanMeter=$request->kondisi_stan_meter;
        $catatanStanMeter=$request->catatan_stan_meter;
        $tanggalAngkat=$request->tanggal_angkat;
        $noMeter=$request->no_meter;
        $ukuranMeter=$request->ukuran_meter;
        $angkaAngkat=$request->angka_angkat;
        $merkMeter=$request->merk_meter;
        $tanggalRealisasi=date('Y-m-d');
        $batdId=$request->batd_id;
        $pelanggaranId=$request->pelanggaran_id;
        $hasil=$request->hasil;
        $picture1 = $request->file('pict1');
        $picture2 = $request->file('pict2');
        $picture3 = $request->file('pict3');
        $picture4 = $request->file('pict4');

        $sendingParams = [
            'id'=>$realisasiId,
            'kondisi_stan_meter' => $kondisiStanMeter,
            'catatan_stan_meter' => $catatanStanMeter,
            'tanggal_angkat'=>$tanggalAngkat,
            'no_meter'=>$noMeter,
            'ukuran_meter'=>$ukuranMeter,
            'angka_angkat'=>$angkaAngkat,
            'merk_meter'=>$merkMeter,
            'tanggal_realisasi'=>$tanggalRealisasi,
            'batd_id'=>$batdId,
            'pelanggaran_id'=>$pelanggaranId,
            'hasil'=>$hasil
        ];

        if(is_null($kondisiStanMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter kondisi_stan_meter!',json_encode($sendingParams));
        }
        if(is_null($catatanStanMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter catatan_stan_meter!',json_encode($sendingParams));
        }
        if(is_null($tanggalAngkat)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter tanggal_angkat!',json_encode($sendingParams));
        }
        if(is_null($noMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter no_meter!',json_encode($sendingParams));
        }
        if(is_null($ukuranMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter ukuran_meter!',json_encode($sendingParams));
        }
        if(is_null($angkaAngkat)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter angka_angkat!',json_encode($sendingParams));
        }
        if(is_null($merkMeter)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter merk_meter!',json_encode($sendingParams));
        }
        if(is_null($hasil)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Missing required parameter hasil!',json_encode($sendingParams));
        }

        $checkBatd=Batd::where(['id'=>$batdId])->first();
        if(is_null($checkBatd)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Batd not found!',json_encode($sendingParams));
        }
        $checkPelanggaran=Pelanggaran::where(['id'=>$pelanggaranId])->first();
        if(is_null($checkPelanggaran)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Pelanggaran not found!',json_encode($sendingParams));
        }
        $data=Realisasi::find($realisasiId);
        if(is_null($data)){
            return $this->messageSystem->returnApiMessage($apiName,404,'Realisasi not found!',json_encode($sendingParams));
        }

        try{
            $data->kondisi_stan_meter=$kondisiStanMeter;
            $data->catatan_stan_meter=$catatanStanMeter;
            $data->tanggal_angkat=$tanggalAngkat;
            $data->no_meter=$noMeter;
            $data->ukuran_meter=$ukuranMeter;
            $data->angka_angkat=$angkaAngkat;
            $data->merk_meter=$merkMeter;
            $data->hasil=$hasil;
            $data->batd_id=$batdId;
            $data->pelanggaran_id=$pelanggaranId;
            $data->tanggal_realisasi=$tanggalRealisasi;

            if($data->status_revisi==1 && $data->status_baru==0){
                $data->status_revisi=2;
            }

            if(!empty($picture1)||!empty($picture2)||!empty($picture3)||!empty($picture4)) {
                    $destinationPath = 'public/uploads/foto-realisasi/';
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
            }
            if(!is_null($picture1)){
                $filename1 = date("YmdHis"). '-' . $picture1->getClientOriginalName();
                $picture1->move($destinationPath,$filename1);
                $data->pict1=$filename1;
                
            }
            if (!is_null($picture2)) {
                $filename2 = date("YmdHis"). '-' . $picture2->getClientOriginalName();
                $picture2->move($destinationPath,$filename2);
                $data->pict2=$filename2;
            }
            if(!is_null($picture3)){
                $filename3 = date("YmdHis"). '-' . $picture3->getClientOriginalName();
                $picture3->move($destinationPath,$filename3);
                $data->pict3=$filename3;
                
            }
            if(!is_null($picture4)){
                $filename4 = date("YmdHis"). '-' . $picture4->getClientOriginalName();
                $picture4->move($destinationPath,$filename4);
                $data->pict4=$filename4;
            }

            $data->save();

            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Update Realisasi success!',
                'data' => $data
            ];
            return response()->json($params);

        }catch (\Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

    }


    public function countBaru($petugasId){
        $sendingParams=[
            'petugas_id'=>$petugasId
        ];
        $cekPetugas=Spk::where(['petugas_id'=>$petugasId]);
        $spkId=$cekPetugas->pluck('id');

        $Kirim=Realisasi::where(['status_baru'=>1])->first();
        $cekKirim=Realisasi::where(['status_baru'=>1])->get();

        $baru=Batd::where(['spk_id'=>$spkId])->get();
        $checkRealisasi = 0;
        foreach ($baru as $key => $item) {
            $cekBaru=Realisasi::where(['batd_id'=>$item->id])->first();
            if(is_null($cekBaru)) {
                $checkRealisasi++;
            }
            
        }

        if($Kirim){
            foreach ($cekKirim as $item){
                $id_batdK[]=$item->batd_id;
            }
            $dataKirim=Batd::whereIn('id',$id_batdK)->where(['spk_id'=>$spkId])->count();
            $params = [
                'code' => 302,
                'description' => 'found',
                'message' => 'Jumlah realisasi berhasil di dapatkan',
                'baru' => $checkRealisasi,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }else{
            $params = [
            'code' => 302,
            'description' => 'found',
            'message' => 'Jumlah realisasi berhasil di dapatkan',
            'baru' => 0,
            'kirim' => 0
            ];
            return response()->json($params);
        }      
    }

    public function countRevisi($petugasId){
        $sendingParams=[
            'petugas_id'=>$petugasId
        ];
        $cekPetugas=Spk::where(['petugas_id'=>$petugasId]);
        $spkId=$cekPetugas->pluck('id');

        $Baru=Realisasi::where(['status_revisi'=>1])->first();
        $Kirim=Realisasi::where(['status_revisi'=>2])->first();

        $cekBaru=Realisasi::where(['status_revisi'=>1])->get();
        $cekKirim=Realisasi::where(['status_revisi'=>2])->get();

        if($Baru && $Kirim){
            foreach ($cekBaru as $item){
                $id_batdB[]=$item->batd_id;
            }
            
            foreach ($cekKirim as $item){
                $id_batdK[]=$item->batd_id;
            }
            $dataBaru=Batd::whereIn('id',$id_batdB)->where(['spk_id'=>$spkId])->count();
            $dataKirim=Batd::whereIn('id',$id_batdK)->where(['spk_id'=>$spkId])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'message' => 'Jumlah realisasi revisi berhasil di dapatkan',
                'baru' => $dataBaru,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }


        elseif($Baru){
            foreach ($cekBaru as $item){
                $id_batdB[]=$item->batd_id;
            }
            $dataBaru=Batd::whereIn('id',$id_batdB)->where(['spk_id'=>$spkId])->count();
            $params = [
                'code' => 302,
                'description' => 'found',
                'message' => 'Jumlah realisasi revisi berhasil di dapatkan',
                'baru' => $dataBaru,
                'kirim' => 0
            ];
            return response()->json($params);
        }

        elseif($Kirim){
            foreach ($cekKirim as $item){
                $id_batdK[]=$item->batd_id;
            }
            $dataKirim=Batd::whereIn('id',$id_batdK)->where(['spk_id'=>$spkId])->count();

            $params = [
                'code' => 302,
                'description' => 'found',
                'message' => 'Jumlah realisasi revisi berhasil di dapatkan',
                'baru' => 0,
                'kirim' => $dataKirim
            ];
            return response()->json($params);
        }

        $params = [
            'code' => 302,
            'description' => 'found',
            'message' => 'Jumlah realisasi revisi berhasil di dapatkan',
            'baru' => 0,
            'kirim' => 0
        ];
        return response()->json($params);


    }
}
<?php


namespace App\Http\Controllers\Api;
use App\Clasess\MessageSystemClass;
use App\Http\Controllers\Controller;
use App\Models\Pelanggaran;

class ApiPelanggaranController extends Controller
{

    private  $messageSystem;

    public  function __construct()
    {
        $this->messageSystem= new MessageSystemClass();
    }


    public function index(){
        $data = Pelanggaran::orderBy('kode_pelanggaran', 'ASC')->get();

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get pelanggaran Success!',
            'data' => $data
        ];

        return response()->json($params);
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/04/18
 * Time: 21.18
 */

namespace App\Http\Controllers;
use App\Clasess\CaptchaClass;
use App\Models\Petugas;
use App\Models\User;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{

    private $captcha;

    public function __construct()
    {
        $this->captcha=new CaptchaClass();

    }

    public function index(){
        if (!empty(session('activeUser'))) {
            return redirect('/backend');
        }
        $params=[
            'captcha'=>$this->captcha->createCaptcha()
        ];
        return view('login.index',$params);
    }

    public  function login(Request $request){
        $nip=$request->input('nip');
        $password=$request->input('password');
        $captcha=$request->input('captcha');

        $activeUser=Petugas::where(['nip'=>$nip])->first();
        if(is_null($activeUser)){
            return "<div class='alert alert-danger'>Pengguna Tidak Ditemukan!</div>";

        }else{
            if($activeUser->password !=sha1($password)){
                return "<div class='alert alert-danger'>Username atau Password Salah!</div>";
            }
            if($captcha!=$request->session()->get('captchaCode')){
                return "<div class='alert alert-danger'>Captcha tidak valid!<script>scrollToTop(); reload(1000);</script></div>";
            }
            else{
                $request->session()->put('activeUser', $activeUser);

                return "
            <div class='alert alert-success'>Login berhasil!</div>
            <script> scrollToTop(); reload(1000); </script>";
            }
        }


    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }


}
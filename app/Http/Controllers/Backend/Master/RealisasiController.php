<?php

namespace App\Http\Controllers\Backend\Master;


use App\Clasess\GenerateCodeClass;
use App\Http\Controllers\Controller;
use App\Models\Spk;
use App\Models\Batd;
use App\Models\Petugas;
use App\Models\Realisasi;
use Illuminate\Http\Request;

class RealisasiController extends Controller
{

    private  $generateCode;

    public function __construct()
    {
        $this->generateCode=new GenerateCodeClass();
    }

    public function  index(){
        $data=Realisasi::all();
        $params=[

            'title'=>'Realisasi Filtering',
            'data' => $data
        ];

        return view('backend.data.realisasi.index',$params);
    }

    public function detail(Request $request){
        $realisasiId=$request->get('id');
        $dataRealisasi=Realisasi::where(['id'=>$realisasiId])->first();
        $dataBatd=Batd::where(['id'=>$dataRealisasi->batd_id])->first();

        $params=[
            'title' =>'Detail Realisasi',
            'dataRealisasi'=>$dataRealisasi,
            'dataBatd'=>$dataBatd,
        ];

        return view('backend.data.realisasi.detail',$params);
    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Realisasi::find($id)->delete();
            return "
            <div class='alert alert-success'>Data Pelanggan berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Pelanggan gagal dihapus!</div>";
        }
    }

    public  function formRevisi(Request $request){

        $id = $request->input('id');
        $data = Realisasi::find($id);
        $params = [
            'data' => $data,
        ];
        return view('backend.data.realisasi.formRevisi',$params);
    }

    public  function  saveRevisi(Request $request){
        $id = $request->input('id');
        $data = Realisasi::find($id);
        $data->ket_realisasi = $request->ket_revisi;
        $data->status_revisi = 1;
        $data->status_baru = 0;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data Revisi berhasil disimpan</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Revisi gagal!</div>";
        }

    }

    public function verifikasi(Request $request){
        $id = $request->input('id');
        $data=Realisasi::find($id);
        $data->status_baru =2;
        $data->status_revisi=0;
        try{
            $data->save();
            return "
            <div class='alert alert-success' href=>Verifikasi berhasil</div>
            
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Verifikasi gagal!</div>";
        }
    }

    public function filterRealisasi(Request $request)
    {

        $filterStatus=$request->get('status');
        $columns = array(
            0=> 'id',
            1=> 'status',
            2=> 'tgl',
            3=> 'nomor_batd',
            // 4=> 'keterangan',
            //4=> 'nomor_pelanggan',
            // 6=> 'nama_pelanggan',
            4=> 'detail',
            5=> 'aksi'
        );

        $totalData = Realisasi::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            if(empty($filterStatus) || $filterStatus=='all'){
                $posts = Realisasi::select('realisasi.id',
                    'realisasi.status_baru','realisasi.status_revisi','realisasi.tanggal_realisasi',
                    'batd.nomor_batd')
                    ->join('batd','batd.id','=','batd_id')
                    ->join('pelanggan','pelanggan.id','=','batd.pelanggan_id')
                    //->join('pelanggaran','pelanggaran.id','=','realisasi.pelanggaran_id')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

            }else{
                if($filterStatus<=2){
                    $posts = Realisasi::select('realisasi.id',
                        'realisasi.status_baru','realisasi.status_revisi','realisasi.tanggal_realisasi',
                        'batd.nomor_batd')
                        ->join('batd','batd.id','=','batd_id')
                        ->join('pelanggan','pelanggan.id','=','batd.pelanggan_id')
                        //->join('pelanggaran','pelanggaran.id','=','realisasi.pelanggaran_id')
                        ->where('realisasi.status_baru',$filterStatus)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

                }elseif($filterStatus==3){
                    $posts = Realisasi::select('realisasi.id',
                        'realisasi.status_baru','realisasi.status_revisi','realisasi.tanggal_realisasi',
                        'batd.nomor_batd')
                        ->join('batd','batd.id','=','batd_id')
                        ->join('pelanggan','pelanggan.id','=','batd.pelanggan_id')
                        ->join('pelanggaran','pelanggaran.id','=','realisasi.pelanggaran_id')
                        ->where('realisasi.status_revisi',1)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
                }
                elseif($filterStatus==4){
                    $posts = Realisasi::select('realisasi.id',
                        'realisasi.status_baru','realisasi.status_revisi','realisasi.tanggal_realisasi',
                        'batd.nomor_batd')
                        ->join('batd','batd.id','=','batd_id')
                        ->join('pelanggan','pelanggan.id','=','batd.pelanggan_id')
                        //->join('pelanggaran','pelanggaran.id','=','realisasi.pelanggaran_id')
                        ->where('realisasi.status_revisi',2)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
                }

            }
        }

        //  else {
        //     $search = strtoupper($request->input('search.value'));
        //     if($filterStatus!=3){
        //         $posts = Realisasi::select('realisasi.id',
        //                 'realisasi.status_baru','realisasi.status_revisi','realisasi.tanggal_realisasi',
        //                 'batd.nomor_batd','pelanggan.nomor_pelanggan')
        //                 ->join('batd','batd.id','=','batd_id')
        //                 ->join('pelanggan','pelanggan.id','=','batd.pelanggan_id')
        //                 //->join('pelanggaran','pelanggaran.id','=','realisasi.pelanggaran_id')
        //             ->where('realisasi.status_baru',$filterStatus)
        //             ->offset($start)
        //             ->limit($limit)
        //             ->orderBy($order,$dir)
        //             ->get();

        //         $totalFiltered = Realisasi::select('realisasi.id',
        //                 'realisasi.status_baru','realisasi.status_revisi','realisasi.tanggal_realisasi',
        //                 'batd.nomor_batd','pelanggan.nomor_pelanggan')
        //                 ->join('batd','batd.id','=','batd_id')
        //                 ->join('pelanggan','pelanggan.id','=','batd.pelanggan_id')
        //                 //->join('pelanggaran','pelanggaran.id','=','realisasi.pelanggaran_id')
        //             ->where('realisasi.status_baru',$filterStatus)
        //             ->count();
        //     }else{
        //         $posts =  Realisasi::select('realisasi.id',
        //                 'realisasi.status_baru','realisasi.status_revisi','realisasi.tanggal_realisasi',
        //                 'batd.nomor_batd','pelanggan.nomor_pelanggan')
        //                 ->join('batd','batd.id','=','batd_id')
        //                 ->join('pelanggan','pelanggan.id','=','batd.pelanggan_id')
        //                 //->join('pelanggaran','pelanggaran.id','=','realisasi.pelanggaran_id')
        //             ->where('realisasi.status_revisi',1)
        //             ->offset($start)
        //             ->limit($limit)
        //             ->orderBy($order,$dir)
        //             ->get();

        //         $totalFiltered = Realisasi::select('realisasi.id',
        //                 'realisasi.status_baru','realisasi.status_revisi','realisasi.tanggal_realisasi',
        //                 'batd.nomor_batd','pelanggan.nomor_pelanggan')
        //                 ->join('batd','batd.id','=','batd_id')
        //                 ->join('pelanggan','pelanggan.id','=','batd.pelanggan_id')
        //                 //->join('pelanggaran','pelanggaran.id','=','realisasi.pelanggaran_id')
        //             ->where('realisasi.status_revisi',1)
        //             ->count();
        //     }
        // }

         $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $num=>$post)
            {
                $nestedData['id'] = $num+1;
                $nestedData['status'] = ($post->status_baru==0 && $post->status_revisi==1)?setStatusRevisi($post->status_revisi):setStatusBaru($post->status_baru);
                $nestedData['tgl'] = $post->tanggal_realisasi;
                $nestedData['nomor_batd'] = $post->nomor_batd;
                //$nestedData['keterangan'] = $post->keterangan;
                //$nestedData['nomor_pelanggan'] = $post->nomor_pelanggan;
                //$nestedData['nama_pelanggan'] = $post->nama;
                 $nestedData['detail'] = "<a href='".(url('backend/master/realisasi/detail?id=').''.$post->id.'')."' class='btn btn-primary btn-xs'><i class='fa fa-sign-in'> Detail</i></a>
                     ";
                $nestedData['aksi'] = "
                    <a onclick=deleteData('".($post->id)."') class='btn btn-danger fa fa-trash-o'
                    title='Delete Data'></a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }



}
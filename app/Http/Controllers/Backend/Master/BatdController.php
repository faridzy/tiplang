<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Clasess\GenerateCodeClass;
use App\Models\Batd;
use App\Models\Pelanggan;
use App\Models\Spk;
use Illuminate\Http\Request;

class BatdController extends Controller
{
    private  $generateCode;

    public function __construct()
    {
        $this->generateCode=new GenerateCodeClass();
    }

    public function showBySpk(Request $request){
        $spkId=$request->get('id');
        $dataSpk=Spk::where(['id'=>$spkId])->first();
        $dataBatd=Batd::where(['spk_id'=>$dataSpk->id])->orderBy('nomor_batd','ASC')->get();
        $params=[
            'title' =>'Manajemen BATD ',
            'dataSpk'=>$dataSpk,
            'dataBatd'=>$dataBatd
        ];

        return view('backend.data.batd.index',$params);

    }

    public function form(Request $request){
        $spkId = $request->get('spk_id');
        $id = $request->input('id');
        if($id){
            $data = Batd::find($id);
        }else{
            $data = new Batd();
        }
        $pelanggan=Pelanggan::all();
        $spk = Spk::find($spkId);
        $params = [
            'title' => 'Btd dengan SPK '.$spk->nomor_spk,
            'data' => $data,
            'spk' => $spk,
            'pelanggan'=>$pelanggan,
            'batdCode'=>$this->generateCode->generateBatdCode($data->nomor_batd)
        ];
        return view('backend.data.batd.form',$params);

    }
    public function save(Request $request){

        $id = $request->input('id');
        if($id){
            $data = Batd::find($id);
        }else{
            $data = new Batd();
        }
        $data->pelanggan_id = $request->pelanggan_id;
        $data->spk_id = $request->spk_id;
        $data->nomor_batd=$request->nomor_batd;
        $data->tanggal_batd=$request->tanggal_batd;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal disimpan!</div>";
        }


    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Batd::find($id)->delete();
            return "
            <div class='alert alert-success'>SPK berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! SPK gagal dihapus!</div>";
        }

    }



}
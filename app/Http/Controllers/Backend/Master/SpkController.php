<?php

namespace App\Http\Controllers\Backend\Master;


use App\Clasess\GenerateCodeClass;
use App\Http\Controllers\Controller;
use App\Models\Spk;
use App\Models\Petugas;
use Illuminate\Http\Request;

class SpkController extends Controller
{

    private  $generateCode;

    public function __construct()
    {
        $this->generateCode=new GenerateCodeClass();
    }

    public function  index(){
        $data=Spk::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen SPK'
        ];

        return view('backend.data.spk.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = Spk::find($id);
        }else{
            $data = new Spk();
        }
        $params = [
            'title' => 'Manajemen SPK',
            'data' => $data,
            'spkCode'=>$this->generateCode->generateSpkCode($data->nomor_spk),
            'petugas' => Petugas::all()
        ];
        return view('backend.data.spk.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Spk::find($id);
        }else{
            $data = new Spk();
            $cek=Spk::where(['nomor_spk' => $data->nomor_spk])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! SPK sudah tersedia!</div>";
            }

        }
        $data->nomor_spk = $request->nomor_spk;
        $data->tanggal_spk = $request->tanggal_spk;
        $data->petugas_id = $request->petugas_id;


        try{
            $data->save();
            return "
            <div class='alert alert-success'>SPK berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! SPK gagal disimpan!</div>";
        }

    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Spk::find($id)->delete();
            return "
            <div class='alert alert-success'>SPK berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! SPK gagal dihapus!</div>";
        }

    }
}
<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Petugas;
use Illuminate\Http\Request;

class PetugasController extends Controller
{
    public function  index(){
        $data=Petugas::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Petugas'
        ];

        return view('backend.master.petugas.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = Petugas::find($id);
        }else{
            $data = new Petugas();
        }
        $params = [
            'title' => 'Manajemen Petugas',
            'data' => $data
        ];
        return view('backend.master.petugas.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Petugas::find($id);
        }else{
            $data = new Petugas();
            $cek=Petugas::where(['nip' => $data->nip])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! NIP sudah tersedia!</div>";
            }

        }
        $data->nip = $request->nip;
        $data->nama = $request->nama;
        $data->jabatan = $request->jabatan;
        $data->petugas_role = $request->petugas_role;
        if(!is_null(str_replace(' ','',$request->password))){
            $password = sha1($request->password);
        }else{
            $password=$data->password;
        }
        $data->password=$password;


        try{
            $data->save();
            return "
            <div class='alert alert-success'>Petugas berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Petugas gagal disimpan!</div>";
        }

    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Petugas::find($id)->delete();
            return "
            <div class='alert alert-success'>Petugas berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Petugas gagal dihapus!</div>";
        }

    }
}
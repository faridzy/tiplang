<?php

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Clasess\GenerateCodeClass;
use App\Models\Pelanggaran;
use Illuminate\Http\Request;

class PelanggaranController extends Controller
{
    private  $generateCode;

    public function __construct()
    {
        $this->generateCode=new GenerateCodeClass();
    }
    
    public function  index(){
        $data=Pelanggaran::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Pelanggaran'
        ];

        return view('backend.master.pelanggaran.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = Pelanggaran::find($id);
        }else{
            $data = new Pelanggaran();
        }
        $params = [
            'title' => 'Manajemen Pelanggaran',
            'pelanggaranCode'=>$this->generateCode->generatePelanggaranCode($data->kode_pelanggaran),
            'data' => $data
        ];
        return view('backend.master.pelanggaran.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Pelanggaran::find($id);
        }else{
            $data = new Pelanggaran();
            $cek=Pelanggaran::where(['kode_pelanggaran' => $data->kode_pelanggaran])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! Pelanggaran sudah tersedia!</div>";
            }

        }
        $data->kode_pelanggaran = $request->kode_pelanggaran;
        $data->keterangan = $request->keterangan;


        try{
            $data->save();
            return "
            <div class='alert alert-success'>Kode Pelanggaran berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Pelanggaran gagal disimpan!</div>";
        }
    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Pelanggaran::find($id)->delete();
            return "
            <div class='alert alert-success'>Pelanggaran berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Pelanggaran gagal dihapus!</div>";
        }
    }

}
<?php


namespace App\Http\Controllers\Backend\Master;

use App\Clasess\GenerateCodeClass;
use App\Http\Controllers\Controller;
use App\Models\Pelanggan;
use Illuminate\Http\Request;


class PelangganController extends Controller
{

    private  $generateCode;

    public function __construct()
    {
        $this->generateCode=new GenerateCodeClass();
    }
    
    public function  index(){
        $data=Pelanggan::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Pelanggan'
        ];

        return view('backend.master.pelanggan.index',$params);
    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = Pelanggan::find($id);
        }else{
            $data = new Pelanggan();
        }
        $params = [
            'title' => 'Manajemen Pelanggan',
            'data' => $data,
            'pelangganCode'=>$this->generateCode->generatePelangganCode($data->nomor_pelanggan)
        ];
        return view('backend.master.pelanggan.form',$params);
    }

    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Pelanggan::find($id);
        }else{
            $data = new Pelanggan();
            $cek=Pelanggan::where(['nomor_pelanggan' => $data->nomor_pelanggan])->first();
            if(!is_null($cek)){
                return "<div class='alert alert-danger'>Terjadi kesalahan! Data Pelanggan sudah tersedia!</div>";
            }

        }
        $data->nama = $request->nama;
        $data->zona = $request->zona;
        $data->jalan = $request->jalan;
        $data->no_jalan = $request->no_jalan;
        $data->gang = $request->gang;
        $data->no_tambahan = $request->no_tambahan;
        $data->nomor_pelanggan = $request->nomor_pelanggan;


        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data Pelanggan berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data Pelanggaran gagal disimpan!</div>";
        }
    }

    public function delete(Request $request){
        $id = intval($request->input('id', 0));
        try{
            Pelanggan::find($id)->delete();
            return "
            <div class='alert alert-success'>Data Pelanggan berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Pelanggan gagal dihapus!</div>";
        }
    }

}
<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Spk extends Model
{
    protected $table = 'spk';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getBatd(){

        return $this->hasMany('App\Models\Batd','spk_id','id');
    }

    public function getPetugas(){

        return $this->hasOne('App\Models\Petugas','id','petugas_id');
    }

}
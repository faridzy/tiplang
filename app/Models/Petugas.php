<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/18
 * Time: 22.59
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    protected $table = 'petugas';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
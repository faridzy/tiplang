<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/18
 * Time: 23.00
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Pelanggaran extends Model
{
    protected $table = 'pelanggaran';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
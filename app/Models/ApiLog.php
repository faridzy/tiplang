<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/05/18
 * Time: 00.11
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ApiLog extends Model
{
    protected $table = 'api_logs';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Realisasi extends Model
{
    protected $table = 'realisasi';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getPelanggaran()
    {
        return $this->hasOne('App\Models\Pelanggaran','id','pelanggaran_id');
    }
    public function ambilBatd(){

        return $this->hasOne('App\Models\Batd','id','batd_id');
    }

}
<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Batd extends Model
{
    protected $table = 'batd';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getRealisasi()
    {
        return $this->hasOne('App\Models\Realisasi', 'batd_id', 'id');
    }

    public function getSpk(){
        return $this->hasOne('App\Models\Spk','id','spk_id');
    }

    public function  getPelanggan(){

        return $this->hasOne('App\Models\Pelanggan','id','pelanggan_id');
    }


}
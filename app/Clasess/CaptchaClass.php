<?php

namespace App\Clasess;


use Illuminate\Http\Request;

class CaptchaClass
{


    public function createCaptcha(){

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 6; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
            request()->session()->put('captchaCode',$randomString);
        }
        return $randomString;
    }

}
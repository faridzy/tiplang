<?php


namespace App\Clasess;


use App\Models\Batd;
use App\Models\Spk;
use App\Models\Pelanggan;
use App\Models\Pelanggaran;


class GenerateCodeClass
{
    public function generateSpkCode($code){
        $checkSpkCode=Spk::where(['nomor_spk'=>$code])->first();
        if(is_null($checkSpkCode)){
            $countData=Spk::count();
            if($countData==0){
                $i=1;
            }else{
                $i=$countData+1;
            }
            $number=1000000+$i;
            $generate = "TD".str_pad($number, 6, "0", STR_PAD_LEFT);
            return $generate;
        }
    }

    public function generateBatdCode($code){
        $checkBatdCode=Batd::where(['nomor_batd'=>$code])->first();
        if(is_null($checkBatdCode)){
            $countData=Batd::count();
            if($countData==0){
                $i=1;
            }else{
                $i=$countData+1;
            }
            $number=1000000+$i;
            $generate = "BTD".str_pad($number, 6, "0", STR_PAD_LEFT);
            return $generate;
        }

    }

    public function generatePelangganCode($code){
        $checkPelangganCode=Pelanggan::where(['nomor_pelanggan'=>$code])->first();
        if(is_null($checkPelangganCode)){
            $countData=Pelanggan::count();
            if($countData==0){
                $i=1;
            }else{
                $i=$countData+1;
            }
            $number=1000000+$i;
            $generate = "P".str_pad($number, 6, "0", STR_PAD_LEFT);
            return $generate;
        }

    }

    public function generatePelanggaranCode($code){
        $checkPelanggaranCode=Pelanggaran::where(['kode_pelanggaran'=>$code])->first();
        if(is_null($checkPelanggaranCode)){
            $countData=Pelanggaran::count();
            if($countData==0){
                $i=1;
            }else{
                $i=$countData+1;
            }
            $number=1000000+$i;
            $generate = "PLGR".str_pad($number, 5, "0", STR_PAD_LEFT);
            return $generate;
        }
    }
}
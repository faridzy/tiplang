<li>
    <a href="{{url('/backend')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a>
</li>
<li>
    <a href="#"><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Master</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{url('/backend/master/pelanggan')}}">Pelanggan</a></li>
        <li><a href="{{url('/backend/master/petugas')}}">Petugas</a></li>
        <li><a href="{{url('/backend/master/pelanggaran')}}">Pelanggaran</a></li>

    </ul>
</li>

<li>
    <a href="#"><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{url('/backend/master/spk')}}">Surat Perintah Kerja</a></li>
        <li><a href="{{url('/backend/master/realisasi')}}">Realisasi</a></li>



    </ul>
</li>
<li>
    <a href="{{url('/logout')}}"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a>
</li>
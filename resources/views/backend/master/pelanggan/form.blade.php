<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='nomor_pelanggan' class='col-sm-2 col-xs-12 control-label'>Nomor Pelanggan</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nomor_pelanggan" class="form-control" value="@if(is_null($data->nomor_pelanggan)){{$pelangganCode}}@else{{$data->nomor_pelanggan}}@endif" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Nama</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nama" class="form-control" value="{{$data->nama}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='zona' class='col-sm-2 col-xs-12 control-label'>Zona</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="zona" class="form-control" value="{{$data->zona}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='jalan' class='col-sm-2 col-xs-12 control-label'>Jalan</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="jalan" class="form-control" value="{{$data->jalan}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='no_jalan' class='col-sm-2 col-xs-12 control-label'>Nomor Jalan</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="no_jalan" class="form-control" value="{{$data->no_jalan}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='gang' class='col-sm-2 col-xs-12 control-label'>Gang</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="gang" class="form-control" value="{{$data->gang}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='no_tambahan' class='col-sm-2 col-xs-12 control-label'>Nomor Tambahan</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="no_tambahan" class="form-control" value="{{$data->no_tambahan}}" >
        </div>
    </div>


    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/pelanggan/save', data, '#result-form-konten');
        })
    })
</script>

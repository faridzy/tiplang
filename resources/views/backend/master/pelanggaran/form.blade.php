<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='kode_pelanggaran' class='col-sm-2 col-xs-12 control-label'>Kode Pelanggaran</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="kode_pelanggaran" class="form-control" value="@if(is_null($data->kode_pelanggaran)){{$pelanggaranCode}}@else{{$data->kode_pelanggaran}}@endif" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='keterangan' class='col-sm-2 col-xs-12 control-label'>Keterangan</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="keterangan" class="form-control" value="{{$data->keterangan}}" required="">
        </div>
    </div>


    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/pelanggaran/save', data, '#result-form-konten');
        })
    })
</script>

<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='nip' class='col-sm-2 col-xs-12 control-label'>NIP</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nip" class="form-control" value="{{$data->nip}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Nama</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nama" class="form-control" value="{{$data->nama}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='jabatan' class='col-sm-2 col-xs-12 control-label'>Jabatan</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="jabatan" class="form-control" value="{{$data->jabatan}}" required="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Peran</label>
        <div class="col-lg-10">

            <input type="radio" name="petugas_role" value="1"
                   @if((!empty($data) && $data->petugas_role==1) || empty($data)) checked @endif> Admin

            <input type="radio" name="petugas_role" value="2"
                   @if(!empty($data) && $data->petugas_role==0) checked @endif> Petugas

        </div>
    </div>

    <div class='form-group'>
        <label for='password' class='col-sm-2 col-xs-12 control-label'>Password</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="password" name="password" class="form-control">
        </div>
    </div>



    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/petugas/save', data, '#result-form-konten');
        })
    })
</script>

<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='nomor_spk' class='col-sm-2 col-xs-12 control-label'>Nomor BTD</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nomor_batd" class="form-control" value="@if(is_null($data->nomor_batd)){{$batdCode}}@else{{$data->nomor_batd}}@endif" required="">
        </div>
    </div>

    <div class='form-group'>
        <label for='tanggal_spk' class='col-sm-2 col-xs-12 control-label'>Tanggal BTD</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="date" name="tanggal_batd" class="form-control" value="{{$data->tanggal_batd}}" required="">
        </div>
    </div>

    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Pelanggan</label>
        <div class='col-sm-9 col-xs-12'>
            <select name="pelanggan_id" class="form-control">
                @foreach($pelanggan as $num => $item)
                    <option value="{{$item->id}}" @if(!is_null($data)) @if($item->id == $data->pelanggan_id) selected="selected" @endif @endif >{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
    </div>


    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type="hidden" name="spk_id" value="{{$spk->id}}">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/batd/save', data, '#result-form-konten');
        })
    })
</script>

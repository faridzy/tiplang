<div id="result-form-konten"></div> 
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='nomor_spk' class='col-sm-2 col-xs-12 control-label'>Nomor SPK</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nomor_spk" class="form-control" value="@if(is_null($data->nomor_spk)){{$spkCode}}@else{{$data->nomor_spk}}@endif" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='tanggal_spk' class='col-sm-2 col-xs-12 control-label'>Tanggal SPK</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="date" name="tanggal_spk" class="form-control" value="{{$data->tanggal_spk}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='petugas_id' class='col-sm-2 col-xs-12 control-label'>Petugas</label>
        <div class='col-sm-9 col-xs-12'>
        <select name="petugas_id" class="form-control">
            @foreach($petugas as $num => $item)
                <option value="{{$item->id}}" @if(!is_null($data)) @if($item->id == $data->petugas_id) selected="selected" @endif @endif >{{$item->nama}}</option>
            @endforeach
        </select>
        </div>
    </div>


    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/spk/save', data, '#result-form-konten');
        })
    })
</script>

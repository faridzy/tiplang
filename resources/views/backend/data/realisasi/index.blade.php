@extends('layout.main')
@section('title', $title)
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data Realisasi</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Data</a>
                </li>


                <li class="active">
                    <a>Realisasi</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <div>
                            <select name="filter-realisasi" class="form-control" id="idfilter" onchange="postDataTabe()">
                                <option value="all">Pilih Status Realisasi</option>
                                <option value=1>Realisasi</option>
                                <option value=2>Verifikasi</option>
                                <option value=3>Revisi</option>
                                <option value=4>Revisi Update</option>
                            </select>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="table-role">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Status</th>
                                            <th>Tgl</th>
                                            <th>No Berita Acara</th>
                                            <!-- <th>Pelanggaran</th> -->
                                            <!-- <th>No Pelanggan</th> -->
                                            <!-- <th>Nama Pelanggan</th> -->
                                            <th>Detail</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                       <!--  <tbody>
                                        @foreach($data as $num => $item)
                                            <tr>
                                                <td>{{$num+1}}</td>
                                                <td>
                                                        @if($item->status_revisi==1)
                                                            <a class="btn btn-danger">REVISI</a>
                                                        @endif
                                                        @if($item->status_revisi==2)
                                                            <a class="btn btn-danger">REVISI 2</a>
                                                        @endif
                                                        @if($item->status_baru==1)
                                                            <a class="btn btn-primary">SUDAH REALISASI</a>
                                                        @endif
                                                        @if($item->status_baru==2)
                                                            <a class="btn btn-success">Verifikasi</a>
                                                        @endif
                                                </td>
                                                <td>{{$item->tanggal_realisasi}}</td>
                                                <td>{{$item->ambilBatd->nomor_batd}}</td>
                                                <td>{{$item->getPelanggaran->keterangan}}</td>
                                                <td>{{$item->ambilBatd->getPelanggan->nomor_pelanggan}}</td>
                                                <td>{{$item->ambilBatd->getPelanggan->nama}}</td>

                                               <td><a href="{{url('backend/master/realisasi/detail?id=')}}{{$item->id}}" class="btn btn-info btn-xs">
                                                        <i class="fa fa-sign-in"></i>
                                                        Lihat detail
                                                    </a>
                                                </td>

                                                <td>
                                                    <a onclick="deleteData({{$item->id}})" class="btn btn-danger"
                                                       title="Hapus Data">
                                                      <i class="fa fa-trash-o"></i>     
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>-->
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@section('scripts')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apakah anda yakin akan menghapus data?", function () {
                ajaxTransfer("/backend/master/realisasi/delete", data, "#modal-output");
            })
        }
        function detailData(id) {
            var data=new FormData();
            data.append('id',id);
            ajaxTransfer("backend/master/realisasi/detail",data,"#table-role");
        }

        

        $(document).ready(function () {
            var dataTableRealisasi=$('#table-role').DataTable({

                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ url('/backend/master/realisasi/filter') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}",
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "status"},
                    {"data": "tgl"},
                    {"data": "nomor_batd"},
                    // {"data": "keterangan"},
                    //{"data": "nomor_pelanggan"},
                    // {"data": "nama_pelanggan"},
                    {"data": "detail"},
                    {"data": "aksi"}
                ],
                "pageLength": 6,
                "responsive":true,
                "columnDefs": [
                    {"targets": 0, "orderable": true},
                    // {"targets": 1, "visible": false, "searchable": false},
                ],

            });
            
            $('#idfilter').on('change', function(){
                var filter_value = $(this).val();
                var new_url ="{{url('/backend/master/realisasi/filter?status=')}}"+filter_value;
                dataTableRealisasi.ajax.url(new_url).load();
            });
        })

    </script>
@endsection

@endsection
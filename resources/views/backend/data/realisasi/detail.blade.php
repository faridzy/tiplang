@extends('layout.main')
@section('title', $title)
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Detail Status Realisasi</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Master</a>
                </li>


                <li class="active">
                    <a>Status Realisasi</a>
                </li>
            </ol>
            <br>
           <li class="breadcrumb">
                    <a class="btn btn-success fa fa-arrow-left" href="{{url('/backend/master/realisasi')}}">
                        Back
                    </a>
            </li>
        </div>
        <div class="col-lg-2"></div>
    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        @if($dataRealisasi->status_baru==1)
                            <div class="btn btn-primary" title="Realisasi">Sudah Realisasi</div>
                        @endif
                        @if($dataRealisasi->status_baru==2)
                            <div class="btn btn-success" title="Verifikasi">Verifikasi</div>
                        @endif
                        @if($dataRealisasi->status_revisi==1)
                            <div class="btn btn-danger" title="Revisi">Revisi</div>
                        @endif
                        @if($dataRealisasi->status_revisi==2)
                            <div class="btn btn-danger" title="Revisi Update">Revisi Update</div>
                        @endif
                        <div class="table-responsive">
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table id="table-role" class="table table-striped table-bordered table-hover">
                                        <tr>
                                            <td width="30%" class="font-bold"><h3>Data Realisasi</h3></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nomor BATD</td>
                                            <td>{{$dataBatd->nomor_batd}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Tanggal Serah Berita Acara Tutup Dinas</td>
                                            <td>{{$dataRealisasi->tanggal_realisasi}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Kondisi Stan Meter </td>
                                            <td>{{$dataRealisasi->kondisi_stan_meter}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Catatan Stan Meter</td>
                                            <td>{{$dataRealisasi->catatan_stan_meter}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Tanggal Angkat</td>
                                            <td>{{$dataRealisasi->tanggal_angkat}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nomor Meter</td>
                                            <td>{{$dataRealisasi->no_meter}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Ukuran Meter</td>
                                            <td>{{$dataRealisasi->ukuran_meter}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Angka Angkat</td>
                                            <td>{{$dataRealisasi->angka_angkat}}</td>
                                        </tr>

                                        <tr>
                                            <td width="30%">Merk Meter</td>
                                            <td>{{$dataRealisasi->merk_meter}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Hasil</td>
                                            <td>{{$dataRealisasi->hasil}}</td>
                                        </tr>
                                         @if($dataRealisasi->status_revisi==1)
                                            <tr>
                                                <td width="30%">Keterangan Revisi</td>
                                                <td>{{$dataRealisasi->ket_realisasi}}</td>
                                            </tr>
                                         @endif

                                    </table>


                                    
                                    <table id="table-role" class="table table-striped table-bordered table-hover">
                                        <tr>
                                            <h3><td width="30%" class="font-bold"><h3>Data Pelanggan</h3></td></h3>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nomor Pelanggan</td>
                                            <td>{{$dataBatd->getPelanggan->nomor_pelanggan}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nama Pelanggan</td>
                                            <td>{{$dataBatd->getPelanggan->nama}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Zona</td>
                                            <td>{{$dataBatd->getPelanggan->zona}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Jalan</td>
                                            <td>{{$dataBatd->getPelanggan->jalan}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">No Jalan</td>
                                            <td>{{$dataBatd->getPelanggan->no_jalan}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Gang</td>
                                            <td>{{$dataBatd->getPelanggan->gang}}</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">No Tambahan</td>
                                            <td>{{$dataBatd->getPelanggan->no_tambahan}}</td>
                                        </tr>

                                    </table>

                                    <table>
                                    <div>
                                        <div class="col-md-3">
                                            <div class="thumbnail">
                                                <div class="caption">
                                                    <h2><span class="badge" style="font-size: 14px;">Foto 0%</span></h2>
                                                    <p></p>
                                                    <p class="">
                                                        <a class="fancybox label label-info" rel="gallery1" href="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->pict1)}}" title="0%"><span class="fa fa-search-plus" style="font-size: 12px;"> Zoom</span></a>
                                                    </p>
                                                </div>
                                                <img src="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->pict1)}}" style="width:250px;height:200px;" alt="Belum Upload" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="thumbnail">
                                                <div class="caption">
                                                    <h2><span class="badge" style="font-size: 14px;">Foto 100%</span></h2>
                                                    <p></p>
                                                    <p class="">
                                                        <a class="fancybox label label-info" rel="gallery1" href="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->pict2)}}" title="100%"><span class="fa fa-search-plus" style="font-size: 12px;"> Zoom</span></a>
                                                    </p>
                                                </div>
                                                <img src="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->pict2)}}" style="width:250px;height:200px;" alt="Belum Upload" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="thumbnail">
                                                <div class="caption">
                                                    <h2><span class="badge" style="font-size: 14px;">Rumah</span></h2>
                                                    <p></p>
                                                    <p class="">
                                                        <a class="fancybox label label-info" rel="gallery1" href="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->pict3)}}" title="Zoom"><span class="fa fa-search-plus" style="font-size: 12px;"> Zoom</span></a>
                                                    </p>
                                                </div>
                                                <img src="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->pict3)}}" style="width:250px;height:200px;" alt="Belum Upload" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="thumbnail">
                                                <div class="caption">
                                                    <h2><span class="badge" style="font-size: 14px;">TTD</span></h2>
                                                    <p></p>
                                                    <p class="breadcrumb">
                                                        <a class="fancybox label label-info" rel="gallery1" href="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->pict4)}}" title="TTD"><span class="fa fa-search-plus" style="font-size: 12px;"> Zoom</span></a>
                                                    </p>
                                                </div>
                                                <img src="{{url('/public/uploads/foto-realisasi/'.$dataRealisasi->pict4)}}" style="width:250px;height:200px;" alt="Belum Upload" class="img-responsive">
                                            </div>
                                        </div>
                                    </div>
                                    </table>
                                    
                                                                        
                                    <center>
                                        @if($dataRealisasi->status_baru==1 || $dataRealisasi->status_revisi==1 || $dataRealisasi->status_revisi==2 )
                                            <a style="margin-top: 20px;" onclick="loadModal(this)" target="/backend/master/realisasi/add-revisi" data="id={{$dataRealisasi->id}}"
                                               class="btn btn-danger glyphicon glyphicon-edit" title="Revisi">Revisi</a>
                                        @endif
                                        @if(Session::get('activeUser')->petugas_role==1 && $dataRealisasi->status_baru==1 || $dataRealisasi->status_revisi==2)
                                                <a style="margin-top: 20px;" onclick="verifikasi({{$dataRealisasi->id}})" class="btn btn-success glyphicon glyphicon-check"
                                                       title="Verifikasi">Verifikasi     
                                                    </a>
                                        @endif

                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('scripts')
        <script>
            $(document).ready(function() {
            $(".fancybox").fancybox({
                openEffect  : 'none',
                closeEffect : 'none'
            });
        });

        function verifikasi(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apakah anda yakin akan verifikasi data?", function () {
                ajaxTransfer("/backend/master/realisasi/verifikasi", data, "#modal-output");
            })
        }

        $('.thumbnail').hover(
            function(){
                $(this).find('.caption').slideDown(250); //.fadeIn(250)
            },
            function(){
                $(this).find('.caption').slideUp(250); //.fadeOut(205)
            }
        );
    
    </script>
@endsection

@endsection
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='ket_revisi' class='col-sm-2 col-xs-12 control-label'>Keterangan Revisi</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="ket_revisi" class="form-control" value="" required="">
        </div>
    </div>
    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary  glyphicon glyphicon-check" value="Simpan">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    //revisi
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/realisasi/save-revisi', data, '#result-form-konten');
        })
    })
</script>
